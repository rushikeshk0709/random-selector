package main

import (
	log "random-selector/pkg/logger"

	server "random-selector/internal/httpservice"
)

func main() {
	log.Info("Starting the application...")
	server.Run()
}
