package service

import (
	"math/rand"
	"net/http"
	"strings"

	models "random-selector/models"
	log "random-selector/pkg/logger"

	"github.com/gin-gonic/gin"
)

func SelectOne(ctx *gin.Context) {
	var stringArray []string
	var request = &models.Request{}
	err := ctx.Bind(request)
	if err != nil {
		log.Error(err)
	}
	stringArray = strings.Split(request.Data, ",")

	randomNumber := rand.Intn(len(stringArray))
	var response = &models.Response{}
	response.Data = stringArray[randomNumber]
	ctx.JSON(http.StatusOK, response)
}
