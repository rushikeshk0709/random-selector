package httpservice

import (
	"net/http"
	"os"
	service "random-selector/internal/service"

	"github.com/gin-gonic/gin"
)

func GetRouteAndServer(router *gin.Engine) *http.Server {

	router.GET("/ping", func(ctx *gin.Context) {
		ctx.AbortWithStatus(http.StatusOK)
	})
	router.POST("/random-select", service.SelectOne)

	port := os.Getenv("PORT")
	srv := &http.Server{
		Addr:    ":" + port,
		Handler: router,
	}
	return srv
}
