package httpservice

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	log "random-selector/pkg/logger"
	"time"

	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
)

func Run() {
	ginEngine := gin.Default()

	ginEngine.Use(CorsMiddleware())

	routerServer := GetRouteAndServer(ginEngine)

	go func() {
		// service connections
		if err := routerServer.ListenAndServe(); err != nil {
			log.Error(fmt.Sprintf("listen: %v\n", err))
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Info("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := routerServer.Shutdown(ctx); err != nil {
		log.Error(fmt.Sprintf("Server Shutdown: %v\n", err))
	}
	log.Info("Server exiting")
}

func CorsMiddleware() gin.HandlerFunc {
	corsConfig := cors.Config{
		Origins:         "*", //cfg.Origins,
		Methods:         "GET, PUT, POST, DELETE",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		MaxAge:          50 * time.Second,
		ValidateHeaders: true, //Should be true for production. - is more secure because we validate headers as opposed to ember.
	}

	return cors.Middleware(corsConfig)
}
