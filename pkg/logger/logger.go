package logger

import (
	"log"
	"os"
)

var (
	WarningLogger *log.Logger
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
)

type LogInterface interface {
	Info(interface{})
	Warning(interface{})
	Error(interface{})
}

func init() {
	InfoLogger = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	WarningLogger = log.New(os.Stdout, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(os.Stdout, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

// Info : For Information related logs
func Info(stringValue interface{}) {
	InfoLogger.Println(stringValue)
}
func Warning(stringValue interface{}) {
	WarningLogger.Println(stringValue)
}

func Error(stringValue interface{}) {
	ErrorLogger.Println(stringValue)
}
